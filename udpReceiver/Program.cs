﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using System.Net.Sockets;
using System.Net;


namespace udpReceiver
{
    class Program
    {
        const int port = 11000; // define > init

        static void Main(string[] args)
        {
            // UDP init stuff-----------------
            UdpClient listener = new UdpClient( port );
            IPEndPoint anyIP = new IPEndPoint( IPAddress.Any, 0 );

            // SQL init stuff-----------------
            string connectionString = @"Data Source=loganpc;Initial Catalog=CBRN_Data;User ID=Logan2;Password=asdf;Trusted_Connection=True";
            SqlConnection cnn = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();

            bool done = false;
            string received_data;
            byte[] receive_byte_array;
            try
            {
                while ( !done )
                {
                    Console.WriteLine( "Waiting for broadcast" );
                    receive_byte_array = listener.Receive( ref anyIP );

                    cnn.Open();
                    //string sql = "Insert into Combinator ( UserID, Task) values(546,'" + "ffdassdd" + "')";

                    Console.WriteLine( "Received a broadcast from {0}", anyIP.ToString() );
                    received_data = Encoding.ASCII.GetString( receive_byte_array, 0, receive_byte_array.Length );
                    adapter.InsertCommand = new SqlCommand(received_data, cnn);
                    adapter.InsertCommand.ExecuteNonQuery();
                    cnn.Close();

                    Console.WriteLine( "data follows \n{0}\n\n", received_data );
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine("got me an exception!----------------------------------------------------------");
                Console.WriteLine( e.ToString() );
                cnn.Close();

            }

            listener.Close();
            cnn.Close();

            Console.ReadLine();
        }
    }
}
